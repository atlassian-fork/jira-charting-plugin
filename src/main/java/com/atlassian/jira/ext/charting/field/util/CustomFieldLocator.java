package com.atlassian.jira.ext.charting.field.util;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;

import java.util.List;


public class CustomFieldLocator
{
    public static CustomField getCustomField(CustomFieldManager customFieldManager, Class customFieldTypeClass)
            throws IllegalStateException
    {
        List<CustomField> customFieldObjects = customFieldManager.getCustomFieldObjects();
        for (CustomField customField : customFieldObjects)
        {
            if (customFieldTypeClass.isAssignableFrom(customField.getCustomFieldType().getClass()))
            {
                return customField;
            }
        }
        
        throw new IllegalStateException("Could not locate custom field of type " + customFieldTypeClass.getName());
    }
}
