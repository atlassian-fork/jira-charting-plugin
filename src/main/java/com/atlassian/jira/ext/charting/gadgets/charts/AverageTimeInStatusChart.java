package com.atlassian.jira.ext.charting.gadgets.charts;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.Set;

public class AverageTimeInStatusChart extends AverageStatusChart
{
    public AverageTimeInStatusChart(CustomFieldManager customFieldManager, ConstantsManager constantsManager,
            SearchProvider searchProvider, SearchService searchService, TimeZoneManager timeZoneManager,
            VelocityRequestContextFactory velocityRequestContextFactory)
    {
        super(customFieldManager, constantsManager, searchProvider, searchService,
                timeZoneManager, velocityRequestContextFactory);
    }

    public Chart generate(final JiraAuthenticationContext jiraAuthenticationContext,
                          final SearchRequest searchRequest,
                          final Set<String> statusIds,
                          final ChartFactory.PeriodName periodName,
                          final int daysPrevious,
                          final ChartFactory.PeriodName yAxisPeriod,
                          final int width,
                          final int height) throws SearchException, IOException
    {
        boolean isDaily = StringUtils.equalsIgnoreCase(ChartFactory.PeriodName.daily.toString(), yAxisPeriod.toString());
        I18nHelper i18nHelper = jiraAuthenticationContext.getI18nHelper();

        return super.generate(jiraAuthenticationContext, searchRequest, statusIds, periodName, daysPrevious, yAxisPeriod, width, height,
                i18nHelper.getText(isDaily ? "datacollector.days" : "datacollector.hours"),
                i18nHelper.getText(isDaily ? "datacollector.tooltip.duration.days" : "datacollector.tooltip.duration.hours"));
    }

    public Chart generateInline(final JiraAuthenticationContext jiraAuthenticationContext,
            final SearchRequest searchRequest, final Set<String> statusIds, final ChartFactory.PeriodName periodName,
            final int daysPrevious, final ChartFactory.PeriodName yAxisPeriod, final int width, final int height)
            throws SearchException, IOException
    {
        boolean isDaily = StringUtils
                .equalsIgnoreCase(ChartFactory.PeriodName.daily.toString(), yAxisPeriod.toString());
        I18nHelper i18nHelper = jiraAuthenticationContext.getI18nHelper();

        return super.generateInline(jiraAuthenticationContext, searchRequest, statusIds, periodName, daysPrevious,
                yAxisPeriod, width, height, i18nHelper.getText(isDaily ? "datacollector.days" : "datacollector.hours"),
                i18nHelper.getText(isDaily ? "datacollector.tooltip.duration.days"
                        : "datacollector.tooltip.duration.hours"));
    }
}
