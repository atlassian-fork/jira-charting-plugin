package com.atlassian.jira.ext.charting.charts.workloadpie;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.report.AbstractChartReport;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.ext.charting.gadgets.charts.WorkloadPieChart;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.ReaderCache;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraDurationUtils;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.util.profiling.UtilTimerStack;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.ext.charting.gadgets.charts.WorkloadPieChart.KEY_TOTAL_WORK_HOURS;
import static com.atlassian.jira.issue.fields.FieldManager.CUSTOM_FIELD_PREFIX;

public class WorkloadPieReport extends AbstractChartReport
{
    private static final Logger LOG = Logger.getLogger(WorkloadPieReport.class);

    private final CustomFieldManager customFieldManager;
    private final ConstantsManager constantsManager;
    private final IssueIndexManager issueIndexManager;
    private final SearchProvider searchProvider;
    private final SearchService searchService;
    private final FieldManager fieldManager;
    private final FieldVisibilityManager fieldVisibilityManager;
    private final ReaderCache readerCache;
    private final TimeTrackingConfiguration timeTrackingConfiguration;
    private final VelocityRequestContextFactory velocityRequestContextFactory;


    public WorkloadPieReport(JiraAuthenticationContext jiraAuthenticationContext, ApplicationProperties applicationProperties,
            ProjectManager projectManager, SearchRequestService searchRequestService, ChartUtils chartUtils,
            CustomFieldManager customFieldManager, ConstantsManager constantsManager, IssueIndexManager issueIndexManager,
            SearchProvider searchProvider, SearchService searchService, FieldManager fieldManager,
            FieldVisibilityManager fieldVisibilityManager, ReaderCache readerCache,
            TimeTrackingConfiguration timeTrackingConfiguration, VelocityRequestContextFactory velocityRequestContextFactory)
    {
        super(jiraAuthenticationContext, applicationProperties, projectManager, searchRequestService, chartUtils);
        this.customFieldManager = customFieldManager;
        this.constantsManager = constantsManager;
        this.issueIndexManager = issueIndexManager;
        this.searchProvider = searchProvider;
        this.searchService = searchService;
        this.fieldManager = fieldManager;
        this.fieldVisibilityManager = fieldVisibilityManager;
        this.readerCache = readerCache;
        this.timeTrackingConfiguration = timeTrackingConfiguration;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
    }

    public void validate(ProjectActionSupport action, Map params)
    {
        validateProjectOrFilterId(action, params);
        validateTimeTrackingEnabled(action);
    }

    private void validateTimeTrackingEnabled(ProjectActionSupport projectActionSupport)
    {
        if (!timeTrackingConfiguration.enabled())
        {
            projectActionSupport.addErrorMessage(getText("report.workloadpie.enableTimeTracking"));
        }
    }

// Setters and getters need not to be calculated in Clover
///CLOVER:OFF
    public String getText(String key)
    {
        return authenticationContext.getI18nHelper().getText(key);
    }

    public String getCustomFieldLabel(String key)
    {
        return customFieldManager.getCustomFieldObject(key).getNameKey();
    }
///CLOVER:ON

    public String generateReportHtml(ProjectActionSupport action, Map reqParams) throws Exception
    {
        String methodSignature = "WorkloadPieReport.generateReportHtml(ProjectActionSupport,Map):String";
        final JiraDurationUtils jiraDurationUtils = getJiraDurationUtils();

        UtilTimerStack.push(methodSignature);

        Map<String, Object> reportParams = getHashMap();

        try
        {
            reportParams.put("report", this);
            reportParams.put("action", action);
            reportParams.put("user", authenticationContext.getUser());
            reportParams.put("customFieldPrefix", CUSTOM_FIELD_PREFIX);

            // Retrieve the portlet parameters
            String projectOrFilterId = (String) reqParams.get("projectOrFilterId");
            String statisticType = (String) reqParams.get("statistictype");
            String issueTimeType = (String) reqParams.get("issuetimetype");

            try
            {
                if (!applicationProperties.getOption(APKeys.JIRA_OPTION_TIMETRACKING))
                {
                    reportParams.put("errorMessage", getText("report.workloadpie.enableTimeTracking"));
                    throw new IllegalStateException("No time tracking enabled in this project - can't generate workload chart");
                }

                if (StringUtils.isNotBlank(projectOrFilterId))
                {
                    reportParams.put("statisticType", statisticType);
                    reportParams.put("issueTimeType", issueTimeType);

                    Chart workloadPieChart = newWorkloadPieChart(reportParams, projectOrFilterId, statisticType, issueTimeType);

                    reportParams.putAll(workloadPieChart.getParameters());
                    reportParams.put(
                            KEY_TOTAL_WORK_HOURS,
                            jiraDurationUtils.getShortFormattedDuration((Long) workloadPieChart.getParameters().get(KEY_TOTAL_WORK_HOURS))
                    );
                }

                return descriptor.getHtml("view", reportParams);
            }
            catch (ArithmeticException arEx)
            {
                LOG.error("Encountered Arithmetic exception " + arEx.getMessage());

                if (StringUtils.isEmpty(issueTimeType))
                {
                    //Assume timespent, which is the default old behaviour
                    issueTimeType = "timespent";
                }
                reportParams.put("errorMessage", getText("report.workloadpie.arithmeticerror." + issueTimeType));
                return descriptor.getHtml("view", reportParams);

            }
        }
        finally
        {
            UtilTimerStack.pop(methodSignature);
        }
    }

// Don't need to include setter and getter methods into Clover
///CLOVER:OFF
    protected Chart newWorkloadPieChart(Map<String, Object> reportParams,
            String projectOrFilterId, String statisticType, String issueTimeType)
            throws SearchException, IOException
    {
        final WorkloadPieChart chart = new WorkloadPieChart(customFieldManager, fieldManager, constantsManager, issueIndexManager,
                searchProvider, searchService, fieldVisibilityManager,
                readerCache, velocityRequestContextFactory);
        return chart.generateInline(authenticationContext,
                chartUtils.retrieveOrMakeSearchRequest(projectOrFilterId, reportParams),
                statisticType,
                issueTimeType,
                ChartFactory.REPORT_IMAGE_WIDTH,
                ChartFactory.REPORT_IMAGE_HEIGHT);
    }

    protected HashMap<String, Object> getHashMap()
    {
        return new HashMap<String, Object>();
    }

    protected JiraDurationUtils getJiraDurationUtils()
    {
        return ComponentAccessor.getComponentOfType(JiraDurationUtils.class);
    }
///CLOVER:ON
}
