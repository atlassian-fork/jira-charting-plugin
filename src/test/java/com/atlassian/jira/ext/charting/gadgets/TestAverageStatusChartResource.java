package com.atlassian.jira.ext.charting.gadgets;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.HashSet;

public class TestAverageStatusChartResource extends TestCase
{
    private AverageStatusChartResource averageStatusChartResource;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        averageStatusChartResource = new AverageStatusChartResource(null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    public void testIgoogleHackAppliedIfSelectedStatusesAreCommaSeparated()
    {
        assertEquals(
                new HashSet<String>(Arrays.asList("1", "2", "3")),
                averageStatusChartResource.splitStatusIdsAsRequired(
                        Arrays.asList("1,2,3")
                )
        );
    }

    public void testIgoogleNotHackAppliedIfSelectedStatusesAreNotCommaSeparated()
    {
        assertEquals(
                new HashSet<String>(Arrays.asList("1", "2", "3")),
                averageStatusChartResource.splitStatusIdsAsRequired(
                        Arrays.asList("1", "2", "3")
                )
        );
    }
}
