package com.atlassian.jira.ext.charting.gadgets.charts;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.ChartFactory.PeriodName;
import com.atlassian.jira.charts.jfreechart.ChartHelper;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import junit.framework.TestCase;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.urls.XYURLGenerator;
import org.jfree.data.xy.XYDataset;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.*;

import static org.mockito.Mockito.*;

public class AverageStatusChartTestCase extends TestCase
{
    private AverageStatusChart averageStatusChart;
    
    private Chart chartResult;
    
    private int normalizeDaysValue;
    
    private LinkedHashMap<String, Status> linkedHashMap;
    
    private Set<String> statusIds = new HashSet<String>();
    private ChartFactory.PeriodName periodName = ChartFactory.PeriodName.monthly;
    private ChartFactory.PeriodName yAxisPeriod = ChartFactory.PeriodName.daily;
    
    private int daysPrevious = 10;
    private int width = 800;
    private int height = 600;
    
    private String xLabel = "X-Axis";
    private String tooltip = "Description of this chart";
    private String timeInStatusFieldId = "";
    
    @Mock private CustomFieldManager customFieldManager;
    @Mock private ConstantsManager constantsManager;
    @Mock private SearchProvider searchProvider;
    @Mock private SearchService searchService;
    @Mock private TimeZoneManager timeZoneManager;
    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private SearchRequest searchRequest;
    @Mock private CustomField customField;
    @Mock private ChartHelper helper;
    @Mock private JFreeChart chart;
    @Mock private XYPlot plot;
    @Mock private XYItemRenderer xyItemRenderer;
    @Mock private XYLineAndShapeRenderer xyLineAndShapeRenderer;
    @Mock private ApplicationUser applicationUser;
    @Mock private VelocityRequestContextFactory velocityRequestContextFactory;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        normalizeDaysValue = 8;
        linkedHashMap = new LinkedHashMap<String, Status>();
        
        when(customField.getId()).thenReturn(timeInStatusFieldId);
        when(helper.getChart()).thenReturn(chart);
        when(chart.getPlot()).thenReturn(plot);
        when(jiraAuthenticationContext.getUser()).thenReturn(applicationUser);
        
        averageStatusChart = new AverageStatusChart(customFieldManager, constantsManager, 
                searchProvider, searchService, timeZoneManager,
                velocityRequestContextFactory)
        {
            @Override
            protected CustomField getTimeInStatusCustomField()
            {
                return customField;
            }

            @Override
            protected int calculateNormalizedDaysValue(PeriodName periodName,
                    int daysPrevious)
            {
                return normalizeDaysValue;
            }

            @Override
            protected LinkedHashMap<String, Status> getLinkedHashMap()
            {
                return linkedHashMap;
            }

            @Override
            protected SearchRequest getSearchRequest(SearchRequest searchRequest)
            {
                return searchRequest;
            }

            @Override
            protected ChartHelper getChartHelper(String xLabel,
                    XYDataset xyDataset)
            {
                return helper;
            }

            @Override
            protected void setChartDefaults(
                    JiraAuthenticationContext jiraAuthenticationContext,
                    ChartHelper helper)
            {
                // Do nothing
            }
        };

        when(timeZoneManager.getLoggedInUserTimeZone()).thenReturn(TimeZone.getDefault());
    }
    
    public void testGenerateAverageStatusChartWithoutStatusIds() throws SearchException, IOException
    {
        String location = "/confluence";
        String imageMap = "/image.jpg";
        String imageMapName = "Image of Confluence Login";
        
        when(plot.getRenderer()).thenReturn(xyItemRenderer);
        
        when(helper.getLocation()).thenReturn(location);
        when(helper.getImageMap()).thenReturn(imageMap);
        when(helper.getImageMapName()).thenReturn(imageMapName);
        
        chartResult = averageStatusChart.generate(jiraAuthenticationContext, searchRequest, statusIds, 
                periodName, daysPrevious, yAxisPeriod, width, height, xLabel, tooltip);
        
        Map resultParameters = chartResult.getParameters();
        
        assertFalse(resultParameters.isEmpty());
        
        assertEquals(location, resultParameters.get("chart"));
        assertTrue(resultParameters.get(ChartParamKeys.KEY_COMPLETE_DATASET_URL_GENERATOR) instanceof XYURLGenerator);
        assertTrue(resultParameters.get(ChartParamKeys.KEY_COMPLETE_DATASET) instanceof XYDataset);
        assertEquals(daysPrevious, resultParameters.get("daysPrevious"));
        assertEquals(imageMap, resultParameters.get("imagemap"));
        assertEquals(imageMapName, resultParameters.get("imagemapName"));
    }
    
    public void testGenerateAverageStatusChartWithStatusIdsAndOldChartLookAndFeel() throws SearchException, IOException
    {
        String statusIdString = "1001";
        
        statusIds.add(statusIdString);
        
        when(plot.getRenderer()).thenReturn(xyLineAndShapeRenderer);
        
        chartResult = averageStatusChart.generate(jiraAuthenticationContext, searchRequest, statusIds, 
                periodName, daysPrevious, yAxisPeriod, width, height, xLabel, tooltip);
        
        verify(xyLineAndShapeRenderer, atLeastOnce()).setBaseShapesVisible(true);
        verify(xyLineAndShapeRenderer, atLeastOnce()).setBaseShapesFilled(true);
    }
}
