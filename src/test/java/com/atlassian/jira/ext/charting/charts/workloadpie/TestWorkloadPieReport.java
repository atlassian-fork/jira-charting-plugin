package com.atlassian.jira.ext.charting.charts.workloadpie;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.ReaderCache;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.action.ProjectActionSupport;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

public class TestWorkloadPieReport extends TestCase
{
    private WorkloadPieReport workloadPieReport;
    
    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private ApplicationProperties applicationProperties;
    @Mock private ProjectManager projectManager;
    @Mock private SearchRequestService searchRequestService;
    @Mock private ChartUtils chartUtils;
    @Mock private CustomFieldManager customFieldManager;
    @Mock private ConstantsManager constantsManager;
    @Mock private IssueIndexManager issueIndexManager;
    @Mock private SearchProvider searchProvider;
    @Mock private SearchService searchService;
    @Mock private FieldManager fieldManager;
    @Mock private FieldVisibilityManager fieldVisibilityManager;
    @Mock private ReaderCache readerCache;
    @Mock private TimeTrackingConfiguration timeTrackingConfiguration;
    @Mock private VelocityRequestContextFactory velocityRequestContextFactory;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);

        workloadPieReport = new WorkloadPieReport(jiraAuthenticationContext, applicationProperties, projectManager,
                searchRequestService, chartUtils, customFieldManager, constantsManager, issueIndexManager, searchProvider,
                searchService, fieldManager, fieldVisibilityManager, readerCache, timeTrackingConfiguration,
                velocityRequestContextFactory)
        {
            @Override
            protected void validateProjectOrFilterId(ProjectActionSupport projectActionSupport, Map map)
            {
                // Does nothing
            }
            
            @Override
            public String getText(String key)
            {
                return key;
            }
        };
    }

    public void testTimeTrackingNotEnabledErrorShown()
    {
        ProjectActionSupport projectActionSupport = new ProjectActionSupport(null, null)
        {
            @Override
            protected <T> T getComponentInstanceOfType(Class<T> clazz)
            {
                return null;
            }
        };
        workloadPieReport.validate(projectActionSupport, new HashMap<String, Object>());

        assertTrue(projectActionSupport.hasAnyErrors());
        assertEquals(1, projectActionSupport.getErrorMessages().size());
        assertEquals("report.workloadpie.enableTimeTracking", projectActionSupport.getErrorMessages().iterator().next());
    }

    public void testTimeTrackingNotEnabledErrorShownWhenThereIsAnErrorGettingTimeSpentSystemField() throws FieldException
    {
        when(fieldManager.getAllAvailableNavigableFields()).thenThrow(new FieldException("Fake FieldException", new Exception("Fake Exception")));
        
        ProjectActionSupport projectActionSupport = new ProjectActionSupport(null, null)
        {
            @Override
            protected <T> T getComponentInstanceOfType(Class<T> clazz)
            {
                return null;
            }
        };
        workloadPieReport.validate(projectActionSupport, new HashMap<String, Object>());

        assertTrue(projectActionSupport.hasAnyErrors());
        assertEquals(1, projectActionSupport.getErrorMessages().size());
        assertEquals("report.workloadpie.enableTimeTracking", projectActionSupport.getErrorMessages().iterator().next());
    }
}
