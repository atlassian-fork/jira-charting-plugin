package com.atlassian.jira.ext.charting.gadgets.charts;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.jfreechart.ChartHelper;
import com.atlassian.jira.charts.jfreechart.PieChartGenerator;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.index.ManagedIndexSearcher;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.ReaderCache;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.statistics.TwoDimensionalStatsMap;
import com.atlassian.jira.issue.statistics.util.TwoDimensionalTermHitCollector;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraDurationUtils;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.jira.web.bean.StatisticAccessorBean;
import com.atlassian.query.Query;
import junit.framework.TestCase;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.PieDataset;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Map;

import static org.mockito.Mockito.when;

public class WorkloadPieChartTestCase extends TestCase
{
    public static final String KEY_TOTAL_WORK_HOURS = "numIssues";
    
    private WorkloadPieChart workloadPieChart;
    
    private User user;
    
    @Mock private CustomFieldManager customFieldManager;
    @Mock private FieldManager fieldManager;
    @Mock private ConstantsManager constantsManager;
    @Mock private IssueIndexManager issueIndexManager;
    @Mock private SearchProvider searchProvider;
    @Mock private SearchService searchService;
    @Mock private FieldVisibilityManager fieldVisibilityManager;
    @Mock private ReaderCache readerCache;
    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private SearchRequest searchRequest;
    @Mock private JiraDurationUtils jiraDurationUtils;
    @Mock private StatisticAccessorBean statBean;
    @Mock private StatisticsMapper xAxisStatsMapper;
    @Mock private TwoDimensionalStatsMap statisticsMap2d;
    @Mock private TwoDimensionalTermHitCollector hitCollector;
    @Mock private Query query;
    @Mock private I18nHelper i18nHelper;
    @Mock private PieChartGenerator pieChartGenerator;
    @Mock private ChartHelper helper;
    @Mock private JFreeChart chart;
    @Mock private PiePlot piePlot;
    @Mock private ApplicationUser applicationUser;
    @Mock private VelocityRequestContextFactory velocityRequestContextFactory;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        workloadPieChart = new WorkloadPieChart(customFieldManager, fieldManager, constantsManager, issueIndexManager,
                searchProvider, searchService, fieldVisibilityManager, readerCache, velocityRequestContextFactory)
        {
            @Override
            protected JiraDurationUtils getJiraDurationUtils()
            {
                return jiraDurationUtils;
            }

            @Override
            protected SearchRequest getSearchRequest(SearchRequest searchRequest)
            {
                return searchRequest;
            }

            @Override
            protected StatisticAccessorBean getStatisticAccessorBean(
                    SearchRequest searchRequestCopy, ApplicationUser user)
            {
                return statBean;
            }

            @Override
            protected TwoDimensionalStatsMap getTwoDimensionalStatsMap(
                    StatisticsMapper xAxis, StatisticsMapper yAxis)
            {
                return statisticsMap2d;
            }

            @Override
            protected TwoDimensionalTermHitCollector getTwoDimensionalTermHitCollector(
                    TwoDimensionalStatsMap statisticsMap2d)
            {
                return hitCollector;
            }

            @Override
            protected PieChartGenerator getPieChartGenerator(
                    PieDataset consolidatedDataset, I18nHelper i18nBean)
            {
                return pieChartGenerator;
            }
        };
    }
    
    public void testGenerateCurrentEstimatePieChart() throws SearchException, IOException
    {
        int width = 800;
        int height = 600;
        String statisticType = "";
        String issueTimeType = "currentestimate";
        String location = "/downloads/12345";
        String imageMap = "map.image";
        String imageMapName = "pic_456.jpg";
        
        when(jiraAuthenticationContext.getUser()).thenReturn(applicationUser);
        when(applicationUser.getDirectoryUser()).thenReturn(user);
        when(statBean.getMapper(statisticType)).thenReturn(xAxisStatsMapper);
        when(searchRequest.getQuery()).thenReturn(query);
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(pieChartGenerator.generateChart()).thenReturn(helper);
        when(helper.getChart()).thenReturn(chart);
        when(chart.getPlot()).thenReturn(piePlot);
        
        when(helper.getLocation()).thenReturn(location);
        when(helper.getImageMap()).thenReturn(imageMap);
        when(helper.getImageMapName()).thenReturn(imageMapName);
        
        Chart chartResult = workloadPieChart.generate(jiraAuthenticationContext, searchRequest, statisticType, issueTimeType, width, height);
        Map<String, Object> params = chartResult.getParameters();
        
        assertNotNull(chartResult);
        assertNotNull(params);
        assertEquals(location, params.get("chart"));
    }
}
